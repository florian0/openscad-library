
module rounded_cube(width, depth, height, corner_radius) {
   hull() {     
        // 1 --- 2
        // |     |
        // 3 --- 4
        translate([corner_radius / 2, corner_radius / 2, 0]) cylinder(height, corner_radius / 2, corner_radius / 2);

        translate([width - corner_radius / 2, corner_radius / 2, 0]) cylinder(height, corner_radius / 2, corner_radius / 2);

        translate([width - corner_radius / 2, depth - corner_radius / 2, 0]) cylinder(height, corner_radius / 2, corner_radius / 2);

        translate([corner_radius / 2, depth - corner_radius / 2, 0]) cylinder(height, corner_radius / 2, corner_radius / 2);
    }
}


module cylinder_with_hole(h, r1, r2, hole_r, hole_l) {
    difference() {
        cylinder(h, r1, r2);
        translate([0, 0, h - hole_l]) cylinder(hole_l, hole_r, hole_r);
    }
}
