use <primitives.scad>

module pinheader(cols, rows, rm) {
    pinwidth = 0.5;
    stickout_bottom = 5.5;
    stickout_top = 3;
    pinlength = stickout_top + rm + stickout_bottom;
    
    translate([0, 0, -rm])
    for (row = [0 : rows - 1]) {
        for (col = [0 : cols - 1]) {
            translate([row * rm, col * rm, 0]) union() {
                color("slategrey") rounded_cube(rm, rm, rm, 0.5);
                translate([(rm - pinwidth) / 2, (rm - pinwidth) / 2, -stickout_bottom]) color("gold") cube([pinwidth, pinwidth, pinlength]);
            }
        }
    }
}
