use <pinheader.scad>
use <smd_footprints.scad>


arduino_nano_pcb_dimmension = [1.8, 18, 43];

function arduino_nano_pcb_drilling_positions() = [
    [1, 1, 0],
    [arduino_nano_pcb_dimmension[2] - 1, 1, 0],
    [1, arduino_nano_pcb_dimmension[1] - 1, 0],
    [arduino_nano_pcb_dimmension[2] - 1, arduino_nano_pcb_dimmension[1] - 1, 0]
];

module arduino_nano() {    
    pcb_height = arduino_nano_pcb_dimmension[0];
    pcb_width = arduino_nano_pcb_dimmension[1];
    pcb_length = arduino_nano_pcb_dimmension[2];
    rm = 2.54;
    usb_port_overhang = 1.4;
    drill_radius = 1 / 2;
    $fn = 20;
    
    
    translate([0, -pcb_width/2, 0]) union() {
        difference() {
            color("Navy") cube([pcb_length, pcb_width, pcb_height]);
            
            // drillings
            for (p = arduino_nano_pcb_drilling_positions()) {
                translate([0, 0, -1]) translate(p) cylinder(pcb_height + 2, drill_radius, drill_radius);
            }
        }
        
        // Bottom side PCB
        translate([(pcb_length - (rm * 15)) / 2, 0, 0]) {
            pinheader(1, 15, rm);
            translate([0, pcb_width - rm, 0]) pinheader(1, 15, rm);
        }
        
        // Top side PCB
        translate([0, 0, pcb_height]) {
            translate([pcb_length - rm * 2, (pcb_width - rm * 3) / 2, 0]) rotate([0, 180, -90]) pinheader(2, 3, rm);
            
            port_length = 9;
            port_width = 7.5;
            port_height = 4;
            translate([-usb_port_overhang, (pcb_width - port_width) / 2, 0]) color("silver") cube([port_length, port_width, port_height]);
            
            translate([20, pcb_width / 2, 0]) rotate([0, 0, 45]) qfp(32);
        }
    }
    
}

arduino_nano();

