use <terminal_block.scad>

pcb_width = 25.75;
pcb_length = 33.80;
pcb_height = 1.45;
drill_hole_dia = 2.65;
drill_hole_r = drill_hole_dia / 2;


module relais() {
    length = 18.80;
    width = 15.20;
    height = 15.20;
    
    cube([width, length, height]);
}

x1 = 1 + drill_hole_r;
x2 = pcb_width - 1 - drill_hole_r;
y1 = 1 + drill_hole_r;
y2 = pcb_length - 1 - drill_hole_r;

function relais_module_drill_positions() = [
    [x1, y1], [x2, y1], [x1, y2], [x2, y2] 
];

module relais_module() {    
    // PCB
    difference() {
        color("green") cube([pcb_width, pcb_length, pcb_height]);
        
        // Drill holes
        for (p = relais_module_drill_positions()) {
            x = p[0];
            y = p[1];
            translate([x, y, -0.5]) cylinder(5, drill_hole_r, drill_hole_r, $fn = 20);
        }
    }
    
    
    // Components
    translate([0, 0, pcb_height]) union() {
        translate([5.75, 5.80, 0]) relais();    
        translate([5.75, 5.80 + 18.80, 0]) terminal_block(3);
    }  
}
 
relais_module();
