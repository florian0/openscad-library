
module terminal_block(count) {
    height = 6.5;
    height_total = 10;
    height_roof = height_total - height;
    
    length = 5;
    
    width = 7.60;
    
    
    for (i = [0 : count - 1]) {
        color("blue") translate([length * i, 0, 0]) cube([length, width, height]);
        
        //     b --- c
        //    /       \
        //   /         \
        //  a --------- d
        
        b_x = 1.8;
        c_x = b_x + 5;
        
        a = [0, 0];
        b = [b_x, height_roof];
        c = [c_x, height_roof];
        d = [width, 0];
        
        
        difference() {
            color("blue") translate([length * i, 0, height]) rotate([90, 0, 90]) linear_extrude(length) polygon(points = [a, b, c, d]);
       
            translate([length/2 + length * i, (c_x - b_x) / 2 + b_x, height_total - 1]) cylinder(2, 2, 2, $fn = 20);
            
            
            translate([length/2 + length * i - 0.5, (c_x - b_x) / 2 + b_x - 2, height_total - 2])
            intersection() {
                translate([0, -1, 0]) cube([1, 6, 4]);
                translate([0.5, 2, 0]) cylinder(2, 2, 2, $fn = 20);
            }
        }
        
        translate([length / 2 + length * i, width / 2, -5]) cube([1, 1, 5]);
    }
}

terminal_block(3);
