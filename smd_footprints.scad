
module qfp(pins) {
    pins_per_side = pins / 4;
    pin_width = 0.4;
    pin_pitch = pin_width;
    pinrow_width = (pin_width + pin_pitch) * pins_per_side - pin_width;
    
    package_width = 6.85;
    package_height = 1;
    
    translate([-package_width / 2, -package_width / 2]) {
        color("DarkSlateGray") cube([package_width, package_width, package_height]);
        
        for (side = [[0, -pin_width], [0, package_width], [90, 0], [90, -(package_width + pin_width)]]) {
            for (pin = [0 : pins_per_side - 1]) {
                rotate([0, 0, side[0]])
                translate([(package_width - pinrow_width) / 2, side[1], 0]) 
                translate([pin * (pin_width + pin_pitch), 0, 0]) 
                color("silver") cube(pin_width);
            }
        }
    }
}

